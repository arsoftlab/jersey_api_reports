
package jettyrest;

import org.xhtmlrenderer.util.FSImageWriter;
import sync.Runner;
import sync.UserRegistrationService;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



/**
 * @author artiomcom
 *
 */

//@Path("/registrations")
@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
//@Component(service = Object.class)
public class UserRegistrationResource
{
	private volatile UserRegistrationService userRegistrationService;
	Runner runner = new Runner();
	private Date date1;



	@Path("reports/image/{uuid}")
	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	@Produces("application/pdf")
	@Produces({"image/png", "image/jpeg", "image/gif"})
//	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
//	@Produces({"application/xml", "application/json"})
//	@Produces({MediaType.APPLICATION_JSON, "text/json"})
	public Response getImageReport(@PathParam("uuid") final String uuid)
	{
		String[] args = {""};
		FSImageWriter asd = runner.produceImageFromReport(runner.connectJSoup(uuid));
		System.out.println("image saved");
		File file = new File("88.png");
		return Response.ok(file) //200
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.allow("OPTIONS").build();
	}

	@Path("reports/pdf/{uuid}")
	@GET
	@Produces("application/pdf")
	public Response getPdfReport(@PathParam("uuid") final String uuid)
	{
		FileOutputStream file = null;
		String[] args = {""};

		file = runner.producePDFFromReport((runner.connectJSoup(uuid)));

		File file1 = new File("ConvertedFile.pdf");
		return Response.ok(file1)
				.header("Access-Control-Allow-Origin", "*")
				.header("Content-Disposition",
						"attachment; filename=Otchet_" + uuid + ".pdf")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.allow("OPTIONS").build();
	}
}

	