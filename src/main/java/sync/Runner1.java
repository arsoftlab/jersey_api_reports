package sync;

import java.awt.Font;
import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
//import java.nio.file.Files;
//import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities.EscapeMode;
import org.jsoup.parser.Parser;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.resource.FSEntityResolver;
import org.xhtmlrenderer.swing.Java2DRenderer;
import org.xhtmlrenderer.util.FSImageWriter;
import org.xml.sax.InputSource;

//import com.lowagie.text.pdf.BaseFont;

public class Runner1 {
	
	private static final String IMAGE_DESTINATION_FOLDER = ".";


	public static void main(String[] args) {
		 //replace it with your URL 
//        String strURL = "http://localhost:63342/untitlephp/test.php?link='2016-11-06'";
        
     // render
        Document doc = null;
       
        StringBuilder xmlContent = new StringBuilder();
        
        Response response;
		try {
			response = Jsoup.connect("http://localhost:63342/untitlephp/test.php?date=2016-11-06")
//			        .ignoreContentType(true)
			        .userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")  
//			        .referrer("http://www.google.com")
			        .timeout(1200) 
			        .followRedirects(true)
			        .execute();
			Document doc1 = Jsoup.parse(response.body(), "", Parser.htmlParser());
//			String STYLE= "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"></link>";
//			String STYLE1= "<link rel=\"stylesheet\" type=\"text/css\" href=\"excel-look-alike/excel-2007.css\"></link>";
//			doc1.head().append(STYLE);
//			doc1.head().append(STYLE1);
//			doc = doc1;
			doc = response.parse();
			doc.outputSettings().escapeMode(EscapeMode.xhtml);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
//			System.out.println(doc.html());
			  BufferedImage buff = null;
			W3CDom w3cDom = new W3CDom();
			org.w3c.dom.Document w3cDoc = w3cDom.fromJsoup(doc);
			org.w3c.dom.Document doc2 = null;
//			xmlContent.append(doc.html());
			xmlContent.append(getStringFromDocument(w3cDoc));
			

			 try {
				saveXmlFile(xmlContent, "1.html");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String content = "";
			try {
//				content = readFile("1.html", StandardCharsets.UTF_8);
//				doc2 = XMLUtil.documentFromFile("2.html");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			org.jsoup.nodes.Document doms = Jsoup.parseBodyFragment(content);
			doms.outputSettings().escapeMode(EscapeMode.xhtml); 
			
			try {
				Map<Key,Object> renderingHints = new HashMap<Key,Object>();
				renderingHints.put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
				renderingHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
				renderingHints.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				renderingHints.put(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
				RenderingHints hints = new RenderingHints(renderingHints);
				Java2DRenderer renderer = new Java2DRenderer(createDocumentFromXMLString(xmlContent.toString()), 1378,-1);
				BufferedImage img = renderer.getImage();
				renderer.setRenderingHints(renderingHints);
				renderer.getSharedContext().setDPI(150);
				renderer.setBufferedImageType(BufferedImage.TYPE_INT_ARGB);
		        // write it out, full size, PNG
		        // FSImageWriter instance can be reused for different ../images,
		        // defaults to PNG
		        FSImageWriter imageWriter = new FSImageWriter();
		        final File outputFile = new File("88.png");
		        if (outputFile.exists() && !outputFile.delete()) {
		            throw new RuntimeException(
		                    "On rendering image, could not delete new output file (.delete failed) " +
		                            outputFile.getAbsolutePath()
		            );
		        }
		        String fileName = outputFile.getPath();
		        imageWriter.write(img, fileName);
		    } catch (Exception e) {
		        System.err.println("Could not render input file to image, skipping: " + " err: " + e.getMessage());
		    }
			
			
//			  Graphics2DRenderer.renderToImage(new File("1.html").toURI().toURL().toExternalForm(),
//	 	                700, 700);
			 String File_To_Convert = "1.html";
//		        String url = "";
//				try {
//					url = new File(File_To_Convert).toURI().toURL().toString();
//				} catch (MalformedURLException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				
				
//		        //System.out.println("---"+url);
		        String HTML_TO_PDF = "ConvertedFile.pdf";
		        OutputStream os = null;
//			    
				try {
					os = new FileOutputStream(HTML_TO_PDF);
//					org.w3c.dom.Document doc1 = XMLUtil.documentFromFile("2.html");
//					
//					 String html = getStringFromDocument(doc1);
//					    int width = 1024, height = 768;
//
//					    BufferedImage image = GraphicsEnvironment.getLocalGraphicsEnvironment()
//					        .getDefaultScreenDevice().getDefaultConfiguration()
//					        .createCompatibleImage(width, height);
//
//					    Graphics graphics = image.createGraphics();
//
//					    JEditorPane jep = new JEditorPane("text/html", html);
//					    jep.setSize(width, height);
//					    jep.print(graphics);
//
//					    ImageIO.write(image, "png", new File("heelo.png"));
//
//					HtmlImageGenerator imageGenerator = new HtmlImageGenerator();
//					imageGenerator.loadHtml(content);
//					
//			        imageGenerator.saveAsImage("hello-world2.jpg");
//			        imageGenerator.saveAsHtmlWithMap("hello-world1.html", "hello-world1.png");
////					Graphics2DRenderer renderer1 = new Graphics2DRenderer();    
				     ITextRenderer renderer1 = new ITextRenderer();
//				     renderer1.getFontResolver().addFont("TimesNewRoman.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
//				     ArrayList<Font> awtFonts = new ArrayList<Font>();
//				        ITextFontResolver resolver = renderer1.getFontResolver();
//
//				        addFonts(false, awtFonts, resolver, "/usr/share/fonts/truetype");
//
//				        ArrayList<String> fontFamilies = new ArrayList<String>();
//				        for (Font awtFont : awtFonts) {
//				            if (!fontFamilies.contains(awtFont.getFamily())) {
//				                fontFamilies.add(awtFont.getFamily());
//				            }
//				        }
				        final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
				        documentBuilderFactory.setValidating(false);
				        DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
				        builder.setEntityResolver(FSEntityResolver.instance());
				        org.w3c.dom.Document document = builder.parse(new ByteArrayInputStream(xmlContent.toString().getBytes()));
				        


//		                renderer1.getSharedContext().setPrint(true);
//		                renderer1.getSharedContext().setDPI(500);
		                renderer1.setDocument(document, null);
		                renderer1.layout();
//		                try {
//		                File outputfile = new File("image.png");
//		    	        try {
//		    	 		ImageIO.write(renderer1.renderToImage("2.html", 700, 500), "png", outputfile);
		    	 		
//		    	 	} catch (IOException e) {
//		    	 		// TODO Auto-generated catch block
//		    	 		e.printStackTrace();
//		    	 	}
							renderer1.createPDF(os) ;
//						} catch (DocumentException e2) {
//							// TODO Auto-generated catch block
//							e2.printStackTrace();
//						}   
					os.close();
//				} catch (FileNotFoundException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}      
			

		     
		               
		          System.out.println("done.");
		          
//			org.w3c.dom.Document doc1;
//		    Graphics2DRenderer renderer = new Graphics2DRenderer();
//			try {
////				doc1 = XMLUtil.documentFromFile("1.html");
//			    renderer.setDocument(w3cDoc, new File("2.html").toURL().toString());
//
//			} catch (Exception e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}

//		    BufferedImage buff1 = new BufferedImage(800, 800, BufferedImage.TYPE_4BYTE_ABGR);
////		    Graphics2D g = (Graphics2D) buff.getGraphics();
//		    
////			Graphics2DRenderer gr = new Graphics2DRenderer();
////	        gr.setDocument(w3cDoc, "2.jpg");
//	        File outputfile = new File("image.jpg");
//	        try {
//	 		ImageIO.write(buff1, "jpg", outputfile);
//	 		
//	 	} catch (IOException e) {
//	 		// TODO Auto-generated catch block
//	 		e.printStackTrace();
//	 	}
////	        gr.layout(g2, dim);
////	        Rectangle minSize = r.getMinimumSize();
//	        //create new image with this size
////	        gr.render(newG2);
	        
	        
			
		
		
		

        
        
       
        
//        WebDriver driver = new org.openqa.selenium.firefox.FirefoxDriver();
//        WebDriver driver;
//        System.setProperty("webdriver.gecko.driver", "geckodriver");
//        driver = new org.openqa.selenium.firefox.FirefoxDriver();
//        driver.get(strURL);
//        Document document = Jsoup.parse(driver.getPageSource());
//       
//        //select all img tags
//        Elements imageElements = document.select("img");
//        
//        //iterate over each image
//        for(Element imageElement : imageElements){
//            
//            //make sure to get the absolute URL using abs: prefix
//            String strImageURL = imageElement.attr("abs:src");
//            
//            //download image one by one
//            downloadImage(strImageURL);
//            
//        }
//	}
//        
//        
//        	private static void downloadImage(String strImageURL){
//            
//            //get file name from image path
//            String strImageName = 
//                    strImageURL.substring( strImageURL.lastIndexOf("/") + 1 );
//            
//            System.out.println("Saving: " + strImageName + ", from: " + strImageURL);
//            
//            try {
//                
//                //open the stream from URL
//                URL urlImage = new URL(strImageURL);
//                InputStream in = urlImage.openStream();
//                
//                byte[] buffer = new byte[4096];
//                int n = -1;
//                
//                OutputStream os = 
//                    new FileOutputStream( IMAGE_DESTINATION_FOLDER + "/" + strImageName );
//                
//                //write bytes to the output stream
//                while ( (n = in.read(buffer)) != -1 ){
//                    os.write(buffer, 0, n);
//                }
//                
//                //close the stream
//                os.close();
//                
//                System.out.println("Image saved");
//                
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            
        }
	
	   public static void saveXmlFile(StringBuilder xmlContent, String saveLocation) throws IOException {
	        FileWriter fileWriter = new FileWriter(saveLocation);
	        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
	        bufferedWriter.write(xmlContent.toString());
	        bufferedWriter.close();
	        System.out.println("Downloading completed successfully..!");
	    }
	   
	 //method to convert Document to String
	   public static String getStringFromDocument(org.w3c.dom.Document doc)
	   {
	       try
	       {
	          DOMSource domSource = new DOMSource(doc);
	          StringWriter writer = new StringWriter();
	          StreamResult result = new StreamResult(writer);
	          TransformerFactory tf = TransformerFactory.newInstance();
	          Transformer transformer = tf.newTransformer();
	          transformer.transform(domSource, result);
	          return writer.toString();
	       }
	       catch(TransformerException ex)
	       {
	          ex.printStackTrace();
	          return null;
	       }
	   } 
	   
	   public static org.w3c.dom.Document createDocumentFromXMLString(String xml) throws Exception
	   {
	   DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
	   fac.setNamespaceAware(false);
	   fac.setValidating(false);
	   fac.setFeature("http://xml.org/sax/features/namespaces", false);
	   fac.setFeature("http://xml.org/sax/features/validation", false);
	   fac.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
	   fac.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
	   DocumentBuilder builder = fac.newDocumentBuilder();
	   InputSource is = new InputSource(new StringReader(xml));
	   return builder.parse(is);
	   }
	   
	   static String readFile(String path, Charset encoding) 
			   throws IOException 
			 {
			   byte[] encoded = Files.readAllBytes(Paths.get(path));
			   return new String(encoded, encoding);
			 }
	   
	   private static void addFonts(boolean printXHtmlFonts, ArrayList<Font> awtFonts, ITextFontResolver resolver, String fontPath) {
	        File[] fontFiles = new File(fontPath).listFiles();
	        for (File fontFile : fontFiles) {
	            if (fontFile.isFile() && fontFile.getName().toLowerCase().endsWith(".ttf")) {
	                try {
	                    Font awtFont = Font.createFont(Font.TRUETYPE_FONT, fontFile);
	                    awtFonts.add(awtFont);
	                    resolver.addFont(fontFile.toString(), true);
	                    if (printXHtmlFonts) {
	                        System.out.println("OK: " + fontFile.getName() + " - " + awtFont.getFamily() + ":" + awtFont.getName());
	                    }
	                } catch (Exception ex) {
	                    System.out.println("ERROR: " + fontFile.getName() + " - " + ex.getMessage());
	                }
	            }
	        }
	   }
}