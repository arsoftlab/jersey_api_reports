package sync;

import com.itextpdf.text.pdf.BaseFont;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities.EscapeMode;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.resource.FSEntityResolver;
import org.xhtmlrenderer.swing.Java2DRenderer;
import org.xhtmlrenderer.util.FSImageWriter;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.RenderingHints.Key;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Runner {

	private static final String IMAGE_DESTINATION_FOLDER = ".";


	public Runner() {
	}
	
	 public static void main(String[] args) {
		producePDFFromReport(connectJSoup("2016-11-06"));
	}
	
	
	public static String connectJSoup(String date){
		Document doc = null;
		StringBuilder xmlContent = new StringBuilder();
		Response response;
		try {
            response = Jsoup.connect("http://localhost:63342/untitlephp/test.php?date=" + date)
					//			        .ignoreContentType(true)
//					.userAgent("Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0")
//								        .referrer("http://www.google.com")
//					.timeout(1200)
//					.followRedirects(true)
					.execute();
			doc = response.parse();
			doc.outputSettings().escapeMode(EscapeMode.xhtml);
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		W3CDom w3cDom = new W3CDom();
		org.w3c.dom.Document w3cDoc = w3cDom.fromJsoup(doc);
		org.w3c.dom.Document doc2 = null;
		//			xmlContent.append(doc.html());
		xmlContent.append(getStringFromDocument(w3cDoc));

//		try {
//			saveXmlFile(xmlContent, "1.html");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		return xmlContent.toString();
		
	}
	public static FileOutputStream producePDFFromReport (String xmlContent) {
		// render
		String HTML_TO_PDF = "ConvertedFile.pdf";
		FileOutputStream os = null;
		try {
			os = new FileOutputStream(HTML_TO_PDF);
			ITextRenderer renderer1 = new ITextRenderer();
			renderer1.getFontResolver().addFont("css/TimesNewRoman.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

			final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			documentBuilderFactory.setValidating(false);
			DocumentBuilder builder = documentBuilderFactory.newDocumentBuilder();
			builder.setEntityResolver(FSEntityResolver.instance());
			org.w3c.dom.Document document = builder.parse(new ByteArrayInputStream(xmlContent.getBytes()));   
			//		                renderer1.getSharedContext().setPrint(true);
			//		                renderer1.getSharedContext().setDPI(500);
			renderer1.setDocument(document, null);
			renderer1.layout();
			renderer1.createPDF(os);
			//						   
			os.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}      

		System.out.println("done.");
		return os;
	}

	public FSImageWriter produceImageFromReport (String xmlContent) {
		String content = "";
		org.jsoup.nodes.Document doms = Jsoup.parseBodyFragment(xmlContent);
		doms.outputSettings().escapeMode(EscapeMode.xhtml);
        FSImageWriter imageWriter = null;

		try {
			Map<Key,Object> renderingHints = new HashMap<Key,Object>();
//			renderingHints.put(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
//			renderingHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
//			renderingHints.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//			renderingHints.put(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_DISABLE);
			RenderingHints hints = new RenderingHints(renderingHints);
			Java2DRenderer renderer = new Java2DRenderer(createDocumentFromXMLString(xmlContent.toString()), 2048, -1);
			BufferedImage img = renderer.getImage();
			renderer.setRenderingHints(renderingHints);
//			renderer.getSharedContext().setDPI(900);
			renderer.setBufferedImageType(BufferedImage.TYPE_INT_ARGB);
			// write it out, full size, PNG
			// FSImageWriter instance can be reused for different ../images,
			// defaults to PNG
            imageWriter = new FSImageWriter();
			final File outputFile = new File("88.png");
			if (outputFile.exists() && !outputFile.delete()) {
				throw new RuntimeException(
						"On rendering image, could not delete new output file (.delete failed) " +
								outputFile.getAbsolutePath()
						);
			}
            outputFile.deleteOnExit();
			String fileName = outputFile.getPath();
			imageWriter.write(img, fileName);


		} catch (Exception e) {
			System.err.println("Could not render input file to image, skipping: " + " err: " + e.getMessage());
		}

        return imageWriter;
		}

	public static void saveXmlFile(StringBuilder xmlContent, String saveLocation) throws IOException {
		FileWriter fileWriter = new FileWriter(saveLocation);
		BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		bufferedWriter.write(xmlContent.toString());
		bufferedWriter.close();
		System.out.println("Downloading completed successfully..!");
	}

	//method to convert Document to String
	public static String getStringFromDocument(org.w3c.dom.Document doc)
	{
		try
		{
			DOMSource domSource = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			return writer.toString();
		}
		catch(TransformerException ex)
		{
			ex.printStackTrace();
			return null;
		}
	} 

	public org.w3c.dom.Document createDocumentFromXMLString(String xml) throws Exception
	{
		DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
		fac.setNamespaceAware(false);
		fac.setValidating(false);
		fac.setFeature("http://xml.org/sax/features/namespaces", false);
		fac.setFeature("http://xml.org/sax/features/validation", false);
		fac.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
		fac.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
		DocumentBuilder builder = fac.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		return builder.parse(is);
	}

	static String readFile(String path, Charset encoding) 
			throws IOException 
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}


	private static void addFonts(boolean printXHtmlFonts, ArrayList<Font> awtFonts, ITextFontResolver resolver, String fontPath) {
		File[] fontFiles = new File(fontPath).listFiles();
		for (File fontFile : fontFiles) {
			if (fontFile.isFile() && fontFile.getName().toLowerCase().endsWith(".ttf")) {
				try {
					Font awtFont = Font.createFont(Font.TRUETYPE_FONT, fontFile);
					awtFonts.add(awtFont);
					resolver.addFont(fontFile.toString(), true);
					if (printXHtmlFonts) {
						System.out.println("OK: " + fontFile.getName() + " - " + awtFont.getFamily() + ":" + awtFont.getName());
					}
				} catch (Exception ex) {
					System.out.println("ERROR: " + fontFile.getName() + " - " + ex.getMessage());
				}
			}
		}
	}
}