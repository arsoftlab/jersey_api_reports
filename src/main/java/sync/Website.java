package sync;

import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


public class Website {

	/**
	 * Represents the default url
	 */
	private static final String URL = "http://localhost:63342/reports/test.php";

	private static Map<String, String> loginCookies = null;


	public static Map<String, String> getLoginCookies() {
		return loginCookies;
	}


	public static void setLoginCookies(Map<String, String> loginCookies) {
		Website.loginCookies = loginCookies;
	}



	/**
	 * Represents the local document of the website
	 */
	private final Document document;

	/**
	 * Constructs the website
	 * @param document	the website document
	 */
	private Website(final Document document) {
		this.document = document;
	}


	public Document getDocument()
	{		
		return this.document;
	}

	/**
	 * Creates an instance of the {@link Website} used for refference and lookup
	 * @param page
	 * @return	the groups list
	 */
	public static final Website grabGroupsFromPage(int page) {
		try {
			return new Website(Jsoup.connect(URL + page).timeout(60 * 1000)
					.userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6").cookies(loginCookies).get());

		}
		catch (SocketTimeoutException e) {
//			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (ConnectException e) {
//			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (MalformedURLException e) {
//			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}
		catch (IOException e) {
//			EventLog.log(Level.SEVERE, Website.class.getName(), e.getMessage());
		}

		return null;
	}

}
